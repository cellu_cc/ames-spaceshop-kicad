EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ARCSS_Breakouts
LIBS:ARCSS_Sensors
LIBS:ARCSS_Electromechanical
LIBS:ARCSS_Fab
LIBS:Stepper-Booster-Pack-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TI_CC2650_Launchpad T1
U 1 1 5AF669FC
P 2150 3350
F 0 "T1" H 2650 1450 60  0000 C CNN
F 1 "TI_CC2650_Launchpad" H 2150 5650 60  0000 C CNN
F 2 "ARCSS-Fab:Launchpad-40-TH" H 2000 4050 60  0001 C CNN
F 3 "" H 2000 4050 60  0001 C CNN
	1    2150 3350
	1    0    0    -1  
$EndComp
$Comp
L JamecoReliapro238538 M1
U 1 1 5AF66A4B
P 7600 1400
F 0 "M1" H 7600 1675 60  0000 C CNN
F 1 "JamecoReliapro238538" H 7600 1625 20  0000 C CNN
F 2 "ARCSS-Fab:1x06-SMD-RA" H 7600 1400 60  0001 C CNN
F 3 "" H 7600 1400 60  0001 C CNN
	1    7600 1400
	1    0    0    -1  
$EndComp
$Comp
L A4953 A1
U 1 1 5AF66A97
P 5100 1850
F 0 "A1" H 5350 1300 60  0000 C CNN
F 1 "A4953" H 5150 2650 60  0000 C CNN
F 2 "SMD_Packages:SOIC-8-N" H 5125 2050 60  0001 C CNN
F 3 "" H 5125 2050 60  0001 C CNN
	1    5100 1850
	1    0    0    -1  
$EndComp
$Comp
L A4953 A2
U 1 1 5AF66B18
P 5100 3950
F 0 "A2" H 5350 3400 60  0000 C CNN
F 1 "A4953" H 5150 4750 60  0000 C CNN
F 2 "SMD_Packages:SOIC-8-N" H 5125 4150 60  0001 C CNN
F 3 "" H 5125 4150 60  0001 C CNN
	1    5100 3950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5AF66CA8
P 4600 4650
F 0 "#PWR01" H 4600 4400 50  0001 C CNN
F 1 "GND" H 4600 4500 50  0000 C CNN
F 2 "" H 4600 4650 50  0001 C CNN
F 3 "" H 4600 4650 50  0001 C CNN
	1    4600 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4550 4600 4550
Wire Wire Line
	4600 4450 4600 4650
Wire Wire Line
	4700 4450 4600 4450
Connection ~ 4600 4550
$Comp
L GND #PWR02
U 1 1 5AF66D22
P 4600 2550
F 0 "#PWR02" H 4600 2300 50  0001 C CNN
F 1 "GND" H 4600 2400 50  0000 C CNN
F 2 "" H 4600 2550 50  0001 C CNN
F 3 "" H 4600 2550 50  0001 C CNN
	1    4600 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2450 4600 2450
Wire Wire Line
	4600 2350 4600 2550
Wire Wire Line
	4700 2350 4600 2350
Connection ~ 4600 2450
Text GLabel 7100 1300 0    60   Input ~ 0
Mot1a
Text GLabel 7100 1500 0    60   Input ~ 0
Mot1b
Text GLabel 7700 1900 3    60   Input ~ 0
Mot2b
Text GLabel 7500 1900 3    60   Input ~ 0
Mot2a
Wire Wire Line
	7500 1800 7500 1900
Wire Wire Line
	7700 1800 7700 1900
Wire Wire Line
	7200 1500 7100 1500
Wire Wire Line
	7200 1300 7100 1300
Text GLabel 5700 2050 2    60   Input ~ 0
Mot1a
Text GLabel 5700 2150 2    60   Input ~ 0
Mot1b
Text GLabel 5700 4150 2    60   Input ~ 0
Mot2a
Text GLabel 5700 4250 2    60   Input ~ 0
Mot2b
Wire Wire Line
	5600 4150 5700 4150
Wire Wire Line
	5600 4250 5700 4250
Wire Wire Line
	5600 2050 5700 2050
Wire Wire Line
	5600 2150 5700 2150
$Comp
L +12V #PWR03
U 1 1 5AF722E7
P 4600 4050
F 0 "#PWR03" H 4600 3900 50  0001 C CNN
F 1 "+12V" H 4600 4190 50  0000 C CNN
F 2 "" H 4600 4050 50  0001 C CNN
F 3 "" H 4600 4050 50  0001 C CNN
	1    4600 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4050 4600 4150
Wire Wire Line
	4600 4150 4700 4150
$Comp
L +12V #PWR04
U 1 1 5AF7233F
P 4600 1950
F 0 "#PWR04" H 4600 1800 50  0001 C CNN
F 1 "+12V" H 4600 2090 50  0000 C CNN
F 2 "" H 4600 1950 50  0001 C CNN
F 3 "" H 4600 1950 50  0001 C CNN
	1    4600 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 1950 4600 2050
Wire Wire Line
	4600 2050 4700 2050
$Comp
L +3.3V #PWR05
U 1 1 5AF723A5
P 4600 1250
F 0 "#PWR05" H 4600 1100 50  0001 C CNN
F 1 "+3.3V" H 4600 1390 50  0000 C CNN
F 2 "" H 4600 1250 50  0001 C CNN
F 3 "" H 4600 1250 50  0001 C CNN
	1    4600 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 1250 4600 1350
Wire Wire Line
	4600 1350 4700 1350
$Comp
L +3.3V #PWR06
U 1 1 5AF723F9
P 4600 3350
F 0 "#PWR06" H 4600 3200 50  0001 C CNN
F 1 "+3.3V" H 4600 3490 50  0000 C CNN
F 2 "" H 4600 3350 50  0001 C CNN
F 3 "" H 4600 3350 50  0001 C CNN
	1    4600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3350 4600 3450
Wire Wire Line
	4600 3450 4700 3450
$Comp
L +3.3V #PWR07
U 1 1 5AF72448
P 1150 1150
F 0 "#PWR07" H 1150 1000 50  0001 C CNN
F 1 "+3.3V" H 1150 1290 50  0000 C CNN
F 2 "" H 1150 1150 50  0001 C CNN
F 3 "" H 1150 1150 50  0001 C CNN
	1    1150 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1150 1150 1450
Wire Wire Line
	1150 1450 1450 1450
$Comp
L GND #PWR08
U 1 1 5AF724A9
P 1350 5150
F 0 "#PWR08" H 1350 4900 50  0001 C CNN
F 1 "GND" H 1350 5000 50  0000 C CNN
F 2 "" H 1350 5150 50  0001 C CNN
F 3 "" H 1350 5150 50  0001 C CNN
	1    1350 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 5050 1350 5050
Wire Wire Line
	1350 4950 1350 5150
Wire Wire Line
	1450 4950 1350 4950
Connection ~ 1350 5050
Text GLabel 5700 3450 2    60   Input ~ 0
Mot_Ctl_2a
Text GLabel 5700 3550 2    60   Input ~ 0
Mot_Ctl_2b
Wire Wire Line
	5600 3450 5700 3450
Wire Wire Line
	5600 3550 5700 3550
Text GLabel 5700 1350 2    60   Input ~ 0
Mot_Ctl_1a
Text GLabel 5700 1450 2    60   Input ~ 0
Mot_Ctl_1b
Wire Wire Line
	5600 1350 5700 1350
Wire Wire Line
	5600 1450 5700 1450
Text GLabel 2950 3950 2    60   Input ~ 0
Mot_Ctl_1a
Text GLabel 2950 3850 2    60   Input ~ 0
Mot_Ctl_1b
Text GLabel 2950 3750 2    60   Input ~ 0
Mot_Ctl_2a
Text GLabel 2950 3650 2    60   Input ~ 0
Mot_Ctl_2b
Wire Wire Line
	2850 3650 2950 3650
Wire Wire Line
	2850 3750 2950 3750
Wire Wire Line
	2850 3850 2950 3850
Wire Wire Line
	2850 3950 2950 3950
Text GLabel 2950 3450 2    60   Input ~ 0
Sensor2
Text GLabel 2950 1650 2    60   Input ~ 0
Sensor1
Text GLabel 2950 1550 2    60   Input ~ 0
Sensor0
Text GLabel 6900 5000 0    60   Input ~ 0
Sensor2
$Comp
L Capacitor C2
U 1 1 5AFA1A60
P 3300 5850
F 0 "C2" H 3400 5750 60  0000 C CNN
F 1 "Capacitor" H 3300 5850 39  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3300 5850 60  0001 C CNN
F 3 "" H 3300 5850 60  0001 C CNN
	1    3300 5850
	1    0    0    -1  
$EndComp
$Comp
L Polarized_Capacitor C3
U 1 1 5AFA1B11
P 3800 5850
F 0 "C3" H 3900 5750 60  0000 C CNN
F 1 "Polarized_Capacitor" H 3800 5850 39  0000 C CNN
F 2 "Capacitors_THT:CP_Radial_D12.5mm_P7.50mm" H 3800 5850 60  0001 C CNN
F 3 "" H 3800 5850 60  0001 C CNN
	1    3800 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5AFA1B8E
P 3800 6050
F 0 "#PWR09" H 3800 5800 50  0001 C CNN
F 1 "GND" H 3800 5900 50  0000 C CNN
F 2 "" H 3800 6050 50  0001 C CNN
F 3 "" H 3800 6050 50  0001 C CNN
	1    3800 6050
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR010
U 1 1 5AFA1BB5
P 3800 5650
F 0 "#PWR010" H 3800 5500 50  0001 C CNN
F 1 "+12V" H 3800 5790 50  0000 C CNN
F 2 "" H 3800 5650 50  0001 C CNN
F 3 "" H 3800 5650 50  0001 C CNN
	1    3800 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5950 3800 6050
Wire Wire Line
	3300 6000 4050 6000
Wire Wire Line
	4050 6000 4050 5900
Wire Wire Line
	4050 5900 4300 5900
Connection ~ 3800 6000
Wire Wire Line
	3800 5650 3800 5750
Wire Wire Line
	3300 5700 4050 5700
Wire Wire Line
	4050 5700 4050 5800
Wire Wire Line
	4050 5800 4300 5800
Connection ~ 3800 5700
Wire Wire Line
	3300 5700 3300 5750
Wire Wire Line
	3300 5950 3300 6000
$Comp
L +3.3V #PWR011
U 1 1 5AFA1E2A
P 2950 5650
F 0 "#PWR011" H 2950 5500 50  0001 C CNN
F 1 "+3.3V" H 2950 5790 50  0000 C CNN
F 2 "" H 2950 5650 50  0001 C CNN
F 3 "" H 2950 5650 50  0001 C CNN
	1    2950 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5AFA1E4D
P 2950 6050
F 0 "#PWR012" H 2950 5800 50  0001 C CNN
F 1 "GND" H 2950 5900 50  0000 C CNN
F 2 "" H 2950 6050 50  0001 C CNN
F 3 "" H 2950 6050 50  0001 C CNN
	1    2950 6050
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C1
U 1 1 5AFA1E70
P 2950 5850
F 0 "C1" H 3050 5750 60  0000 C CNN
F 1 "Capacitor" H 2950 5850 39  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2950 5850 60  0001 C CNN
F 3 "" H 2950 5850 60  0001 C CNN
	1    2950 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 5950 2950 6050
Wire Wire Line
	2950 5650 2950 5750
$Comp
L Resistor R1
U 1 1 5AFA20A2
P 2550 5850
F 0 "R1" H 2550 5750 60  0000 C CNN
F 1 "Resistor" H 2550 5850 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2550 5850 60  0001 C CNN
F 3 "" H 2550 5850 60  0001 C CNN
	1    2550 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 6050 2550 6150
Wire Wire Line
	2550 5550 2550 5650
Wire Wire Line
	2850 1550 2950 1550
Wire Wire Line
	2850 1650 2950 1650
$Comp
L GND #PWR013
U 1 1 5AFDB384
P 5700 1800
F 0 "#PWR013" H 5700 1550 50  0001 C CNN
F 1 "GND" H 5700 1650 50  0000 C CNN
F 2 "" H 5700 1800 50  0001 C CNN
F 3 "" H 5700 1800 50  0001 C CNN
	1    5700 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1750 5700 1750
Wire Wire Line
	5700 1750 5700 1800
$Comp
L GND #PWR014
U 1 1 5AFDB47F
P 5700 3900
F 0 "#PWR014" H 5700 3650 50  0001 C CNN
F 1 "GND" H 5700 3750 50  0000 C CNN
F 2 "" H 5700 3900 50  0001 C CNN
F 3 "" H 5700 3900 50  0001 C CNN
	1    5700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3850 5700 3850
Wire Wire Line
	5700 3850 5700 3900
$Comp
L A1302_HallEffectSensor A3
U 1 1 5AFDBA76
P 7350 3050
F 0 "A3" H 7525 2950 60  0000 C CNN
F 1 "A1302_HallEffectSensor" H 7450 3350 39  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23W_Handsoldering" H 7350 3050 60  0001 C CNN
F 3 "" H 7350 3050 60  0001 C CNN
	1    7350 3050
	1    0    0    -1  
$EndComp
$Comp
L Resistor R2
U 1 1 5AFDBB2C
P 7750 3250
F 0 "R2" H 7750 3150 60  0000 C CNN
F 1 "Resistor" H 7750 3250 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 7750 3250 60  0001 C CNN
F 3 "" H 7750 3250 60  0001 C CNN
	1    7750 3250
	0    1    1    0   
$EndComp
$Comp
L Resistor R3
U 1 1 5AFDBB81
P 7750 3750
F 0 "R3" H 7750 3650 60  0000 C CNN
F 1 "Resistor" H 7750 3750 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 7750 3750 60  0001 C CNN
F 3 "" H 7750 3750 60  0001 C CNN
	1    7750 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	7750 3450 7750 3550
Wire Wire Line
	7650 2950 7750 2950
Wire Wire Line
	7750 2950 7750 3050
$Comp
L GND #PWR015
U 1 1 5AFDBD74
P 6950 3250
F 0 "#PWR015" H 6950 3000 50  0001 C CNN
F 1 "GND" H 6950 3100 50  0000 C CNN
F 2 "" H 6950 3250 50  0001 C CNN
F 3 "" H 6950 3250 50  0001 C CNN
	1    6950 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3250 6950 3150
Wire Wire Line
	6950 3150 7050 3150
$Comp
L +5V #PWR016
U 1 1 5AFDBE8B
P 1350 1150
F 0 "#PWR016" H 1350 1000 50  0001 C CNN
F 1 "+5V" H 1350 1290 50  0000 C CNN
F 2 "" H 1350 1150 50  0001 C CNN
F 3 "" H 1350 1150 50  0001 C CNN
	1    1350 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1150 1350 1250
Wire Wire Line
	1350 1250 1450 1250
$Comp
L +5V #PWR017
U 1 1 5AFDC241
P 6950 2850
F 0 "#PWR017" H 6950 2700 50  0001 C CNN
F 1 "+5V" H 6950 2990 50  0000 C CNN
F 2 "" H 6950 2850 50  0001 C CNN
F 3 "" H 6950 2850 50  0001 C CNN
	1    6950 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2850 6950 2950
Wire Wire Line
	6950 2950 7050 2950
$Comp
L Capacitor C4
U 1 1 5AFDC4A0
P 6800 3050
F 0 "C4" H 6900 2950 60  0000 C CNN
F 1 "Capacitor" H 6800 3050 39  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6800 3050 60  0001 C CNN
F 3 "" H 6800 3050 60  0001 C CNN
	1    6800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2900 6800 2900
Wire Wire Line
	6800 2900 6800 2950
Connection ~ 6950 2900
Wire Wire Line
	6950 3200 6800 3200
Wire Wire Line
	6800 3200 6800 3150
Connection ~ 6950 3200
$Comp
L GND #PWR018
U 1 1 5AFDC7C4
P 7750 4000
F 0 "#PWR018" H 7750 3750 50  0001 C CNN
F 1 "GND" H 7750 3850 50  0000 C CNN
F 2 "" H 7750 4000 50  0001 C CNN
F 3 "" H 7750 4000 50  0001 C CNN
	1    7750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3950 7750 4000
Text GLabel 7850 3500 2    60   Input ~ 0
Sensor1
Wire Wire Line
	7750 3500 7850 3500
Connection ~ 7750 3500
$Comp
L Barrel_Jack J1
U 1 1 5AFDCFA7
P 4600 5900
F 0 "J1" H 4600 6110 50  0000 C CNN
F 1 "Barrel_Jack" H 4600 5725 50  0000 C CNN
F 2 "ARCSS-Fab:PWR-JACK-0.65x2.75mm" H 4650 5860 50  0001 C CNN
F 3 "" H 4650 5860 50  0001 C CNN
	1    4600 5900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4300 6000 4250 6000
Wire Wire Line
	4250 6000 4250 5900
Connection ~ 4250 5900
$Comp
L Conn_01x03 J2
U 1 1 5AFDD6D5
P 6300 5000
F 0 "J2" H 6300 5200 50  0000 C CNN
F 1 "Conn_01x03" H 6300 4800 50  0000 C CNN
F 2 "ARCSS-Fab:1x03-SMD-RA" H 6300 5000 50  0001 C CNN
F 3 "" H 6300 5000 50  0001 C CNN
	1    6300 5000
	1    0    0    -1  
$EndComp
Text GLabel 6000 5000 0    60   Input ~ 0
Sensor0
$Comp
L GND #PWR019
U 1 1 5AFDD749
P 6000 5200
F 0 "#PWR019" H 6000 4950 50  0001 C CNN
F 1 "GND" H 6000 5050 50  0000 C CNN
F 2 "" H 6000 5200 50  0001 C CNN
F 3 "" H 6000 5200 50  0001 C CNN
	1    6000 5200
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR020
U 1 1 5AFDD7EF
P 6000 4800
F 0 "#PWR020" H 6000 4650 50  0001 C CNN
F 1 "+3.3V" H 6000 4940 50  0000 C CNN
F 2 "" H 6000 4800 50  0001 C CNN
F 3 "" H 6000 4800 50  0001 C CNN
	1    6000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4800 6000 4900
Wire Wire Line
	6000 4900 6100 4900
Wire Wire Line
	6000 5000 6100 5000
Wire Wire Line
	6000 5200 6000 5100
Wire Wire Line
	6000 5100 6100 5100
$Comp
L Conn_01x03 J3
U 1 1 5AFDDA87
P 7200 5000
F 0 "J3" H 7200 5200 50  0000 C CNN
F 1 "Conn_01x03" H 7200 4800 50  0000 C CNN
F 2 "ARCSS-Fab:1x03-SMD-RA" H 7200 5000 50  0001 C CNN
F 3 "" H 7200 5000 50  0001 C CNN
	1    7200 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 5AFDDA8E
P 6900 5200
F 0 "#PWR021" H 6900 4950 50  0001 C CNN
F 1 "GND" H 6900 5050 50  0000 C CNN
F 2 "" H 6900 5200 50  0001 C CNN
F 3 "" H 6900 5200 50  0001 C CNN
	1    6900 5200
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR022
U 1 1 5AFDDA94
P 6900 4800
F 0 "#PWR022" H 6900 4650 50  0001 C CNN
F 1 "+3.3V" H 6900 4940 50  0000 C CNN
F 2 "" H 6900 4800 50  0001 C CNN
F 3 "" H 6900 4800 50  0001 C CNN
	1    6900 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4800 6900 4900
Wire Wire Line
	6900 4900 7000 4900
Wire Wire Line
	6900 5000 7000 5000
Wire Wire Line
	6900 5200 6900 5100
Wire Wire Line
	6900 5100 7000 5100
$Comp
L GND #PWR023
U 1 1 5AFDE1B3
P 2550 6150
F 0 "#PWR023" H 2550 5900 50  0001 C CNN
F 1 "GND" H 2550 6000 50  0000 C CNN
F 2 "" H 2550 6150 50  0001 C CNN
F 3 "" H 2550 6150 50  0001 C CNN
	1    2550 6150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 5AFDE279
P 2550 5550
F 0 "#PWR024" H 2550 5300 50  0001 C CNN
F 1 "GND" H 2550 5400 50  0000 C CNN
F 2 "" H 2550 5550 50  0001 C CNN
F 3 "" H 2550 5550 50  0001 C CNN
	1    2550 5550
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 3450 2950 3450
$EndSCHEMATC
