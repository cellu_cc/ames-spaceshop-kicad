EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ARCSS_Breakouts
LIBS:ARCSS_Electromechanical
LIBS:ARCSS_Fab
LIBS:ARCSS_Sensors
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CORTEX_DEBUG_PORT P1
U 1 1 5AD64DDF
P 5700 2150
F 0 "P1" H 6150 1800 60  0000 C CNN
F 1 "CORTEX_DEBUG_PORT" H 5700 2550 39  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch1.27mm_SMD" H 5650 2150 60  0001 C CNN
F 3 "" H 5650 2150 60  0001 C CNN
	1    5700 2150
	1    0    0    -1  
$EndComp
$Comp
L JLink J1
U 1 1 5AD64E24
P 3650 2300
F 0 "J1" H 3650 1750 60  0000 C CNN
F 1 "JLink" H 3650 2850 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x10_Pitch2.54mm_SMD" H 3650 2350 60  0001 C CNN
F 3 "" H 3650 2350 60  0001 C CNN
	1    3650 2300
	1    0    0    -1  
$EndComp
Text GLabel 3100 1900 0    60   Input ~ 0
VTREF
Wire Wire Line
	3200 1900 3100 1900
Text GLabel 3100 2600 0    60   Input ~ 0
RESET
Text GLabel 3100 2500 0    60   Input ~ 0
TDO
Text GLabel 3100 2300 0    60   Input ~ 0
TCK
Text GLabel 3100 2200 0    60   Input ~ 0
TMS
Text GLabel 3100 2100 0    60   Input ~ 0
TDI
Text GLabel 4350 2000 2    60   Input ~ 0
GND
Wire Wire Line
	4150 2000 4350 2000
Wire Wire Line
	4150 2100 4250 2100
Wire Wire Line
	4250 2000 4250 2400
Connection ~ 4250 2000
Wire Wire Line
	4250 2200 4150 2200
Connection ~ 4250 2100
Wire Wire Line
	4250 2300 4150 2300
Connection ~ 4250 2200
Wire Wire Line
	4250 2400 4150 2400
Connection ~ 4250 2300
Wire Wire Line
	3200 2100 3100 2100
Wire Wire Line
	3200 2200 3100 2200
Wire Wire Line
	3200 2300 3100 2300
Wire Wire Line
	3200 2500 3100 2500
Wire Wire Line
	3200 2600 3100 2600
Text GLabel 3100 2800 0    60   Input ~ 0
5V
Wire Wire Line
	3100 2800 3200 2800
Text GLabel 4950 1900 0    60   Input ~ 0
VTREF
Text GLabel 4900 2300 0    60   Input ~ 0
GND
Text GLabel 6450 2200 2    60   Input ~ 0
TDI
Text GLabel 6450 2100 2    60   Input ~ 0
TDO
Text GLabel 6450 1900 2    60   Input ~ 0
TMS
Text GLabel 6450 2000 2    60   Input ~ 0
TCK
Text GLabel 6450 2300 2    60   Input ~ 0
RESET
Wire Wire Line
	6350 1900 6450 1900
Wire Wire Line
	6350 2000 6450 2000
Wire Wire Line
	6450 2100 6350 2100
Wire Wire Line
	6350 2200 6450 2200
Wire Wire Line
	6450 2300 6350 2300
Wire Wire Line
	5050 2300 4900 2300
Wire Wire Line
	5050 1900 4950 1900
$Comp
L Conn_01x02 J2
U 1 1 5AD65140
P 4650 1050
F 0 "J2" H 4650 1150 50  0000 C CNN
F 1 "Conn_01x02" H 4650 850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 4650 1050 50  0001 C CNN
F 3 "" H 4650 1050 50  0001 C CNN
	1    4650 1050
	1    0    0    -1  
$EndComp
Text GLabel 4350 1050 0    60   Input ~ 0
5V
Text GLabel 4350 1150 0    60   Input ~ 0
GND
Wire Wire Line
	4450 1050 4350 1050
Wire Wire Line
	4450 1150 4350 1150
$EndSCHEMATC
