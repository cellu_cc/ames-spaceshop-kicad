EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ARCSS_Breakouts
LIBS:ARCSS_Electromechanical
LIBS:ARCSS_Fab
LIBS:ARCSS_Sensors
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TCS3200D T1
U 1 1 5AD5337D
P 5950 3300
F 0 "T1" H 5950 3650 39  0000 C CNN
F 1 "TCS3200D" H 5950 3750 39  0000 C CNN
F 2 "SMD_Packages:SOIC-8-N" H 5950 3300 60  0001 C CNN
F 3 "" H 5950 3300 60  0001 C CNN
	1    5950 3300
	1    0    0    -1  
$EndComp
$Comp
L Resistor R1
U 1 1 5AD533D6
P 6350 2700
F 0 "R1" H 6350 2600 60  0000 C CNN
F 1 "Resistor" H 6350 2700 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 6350 2700 60  0001 C CNN
F 3 "" H 6350 2700 60  0001 C CNN
	1    6350 2700
	0    1    1    0   
$EndComp
$Comp
L Resistor R3
U 1 1 5AD534B8
P 6600 2700
F 0 "R3" H 6600 2600 60  0000 C CNN
F 1 "Resistor" H 6600 2700 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 6600 2700 60  0001 C CNN
F 3 "" H 6600 2700 60  0001 C CNN
	1    6600 2700
	0    1    1    0   
$EndComp
$Comp
L Resistor R4
U 1 1 5AD534DD
P 6850 2700
F 0 "R4" H 6850 2600 60  0000 C CNN
F 1 "Resistor" H 6850 2700 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 6850 2700 60  0001 C CNN
F 3 "" H 6850 2700 60  0001 C CNN
	1    6850 2700
	0    1    1    0   
$EndComp
$Comp
L Resistor R5
U 1 1 5AD534FA
P 7100 2700
F 0 "R5" H 7100 2600 60  0000 C CNN
F 1 "Resistor" H 7100 2700 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 7100 2700 60  0001 C CNN
F 3 "" H 7100 2700 60  0001 C CNN
	1    7100 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 3000 6400 3000
Wire Wire Line
	6350 3000 6350 2900
Wire Wire Line
	6250 3100 6650 3100
Wire Wire Line
	6600 3100 6600 2900
Wire Wire Line
	6250 3200 6900 3200
Wire Wire Line
	6850 3200 6850 2900
Wire Wire Line
	6250 3300 7150 3300
Wire Wire Line
	7100 3300 7100 2900
$Comp
L VDD #PWR01
U 1 1 5AD535B6
P 6750 2150
F 0 "#PWR01" H 6750 2000 60  0001 C CNN
F 1 "VDD" H 6750 2300 60  0000 C CNN
F 2 "" H 6750 2150 60  0000 C CNN
F 3 "" H 6750 2150 60  0000 C CNN
	1    6750 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2300 6750 2150
Wire Wire Line
	6350 2300 7350 2300
Wire Wire Line
	6850 2300 6850 2500
Wire Wire Line
	7100 2300 7100 2500
Connection ~ 6850 2300
Wire Wire Line
	6600 2300 6600 2500
Connection ~ 6750 2300
Wire Wire Line
	6350 2300 6350 2500
Connection ~ 6600 2300
$Comp
L VDD #PWR02
U 1 1 5AD53662
P 5550 2750
F 0 "#PWR02" H 5550 2600 60  0001 C CNN
F 1 "VDD" H 5550 2900 60  0000 C CNN
F 2 "" H 5550 2750 60  0000 C CNN
F 3 "" H 5550 2750 60  0000 C CNN
	1    5550 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3000 5550 3000
Wire Wire Line
	5550 2750 5550 3200
$Comp
L Resistor R2
U 1 1 5AD5369A
P 6400 3900
F 0 "R2" H 6400 3800 60  0000 C CNN
F 1 "Resistor" H 6400 3900 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 6400 3900 60  0001 C CNN
F 3 "" H 6400 3900 60  0001 C CNN
	1    6400 3900
	0    1    1    0   
$EndComp
$Comp
L Resistor R6
U 1 1 5AD536EA
P 7350 2700
F 0 "R6" H 7350 2600 60  0000 C CNN
F 1 "Resistor" H 7350 2700 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 7350 2700 60  0001 C CNN
F 3 "" H 7350 2700 60  0001 C CNN
	1    7350 2700
	0    1    1    0   
$EndComp
Connection ~ 7100 2300
$Comp
L GND #PWR03
U 1 1 5AD537EB
P 5550 4250
F 0 "#PWR03" H 5550 4000 60  0001 C CNN
F 1 "GND" H 5550 4100 60  0000 C CNN
F 2 "" H 5550 4250 60  0000 C CNN
F 3 "" H 5550 4250 60  0000 C CNN
	1    5550 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3400 5550 4250
Wire Wire Line
	5550 3600 5650 3600
$Comp
L Capacitor C1
U 1 1 5AD5389F
P 5550 3300
F 0 "C1" H 5650 3200 60  0000 C CNN
F 1 "Capacitor" H 5550 3300 39  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5550 3300 60  0001 C CNN
F 3 "" H 5550 3300 60  0001 C CNN
	1    5550 3300
	1    0    0    -1  
$EndComp
Connection ~ 5550 3000
Connection ~ 5550 3600
Text GLabel 6400 3000 2    60   Input ~ 0
S0
Connection ~ 6350 3000
Text GLabel 6650 3100 2    60   Input ~ 0
S1
Text GLabel 6900 3200 2    60   Input ~ 0
S2
Text GLabel 7150 3300 2    60   Input ~ 0
S3
Text GLabel 6500 3400 2    60   Input ~ 0
OE
Connection ~ 7100 3300
Connection ~ 6850 3200
Connection ~ 6600 3100
Text GLabel 7400 3600 2    60   Input ~ 0
OUT
Wire Wire Line
	6250 3600 7400 3600
Wire Wire Line
	7350 3600 7350 2900
Connection ~ 7350 3600
Wire Wire Line
	7350 2300 7350 2500
Wire Wire Line
	6250 3400 6500 3400
Wire Wire Line
	6400 3400 6400 3700
Connection ~ 6400 3400
Wire Wire Line
	6400 4100 6400 4150
Wire Wire Line
	6400 4150 5550 4150
Connection ~ 5550 4150
$Comp
L CONN_01X05 P2
U 1 1 5AD54144
P 6150 1350
F 0 "P2" H 6150 1650 50  0000 C CNN
F 1 "CONN_01X05" V 6250 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 6150 1350 60  0001 C CNN
F 3 "" H 6150 1350 60  0000 C CNN
	1    6150 1350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 P1
U 1 1 5AD54195
P 6050 2100
F 0 "P1" H 6050 2400 50  0000 C CNN
F 1 "CONN_01X05" V 6150 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 6050 2100 60  0001 C CNN
F 3 "" H 6050 2100 60  0000 C CNN
	1    6050 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AD54289
P 5800 1600
F 0 "#PWR04" H 5800 1350 60  0001 C CNN
F 1 "GND" H 5800 1450 60  0000 C CNN
F 2 "" H 5800 1600 60  0000 C CNN
F 3 "" H 5800 1600 60  0000 C CNN
	1    5800 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1550 5800 1550
Wire Wire Line
	5800 1250 5800 1600
Wire Wire Line
	5950 1250 5800 1250
Connection ~ 5800 1550
Text GLabel 5700 1350 0    60   Input ~ 0
OE
Wire Wire Line
	5950 1350 5700 1350
$Comp
L VDD #PWR05
U 1 1 5AD543DF
P 5850 1050
F 0 "#PWR05" H 5850 900 60  0001 C CNN
F 1 "VDD" H 5850 1200 60  0000 C CNN
F 2 "" H 5850 1050 60  0000 C CNN
F 3 "" H 5850 1050 60  0000 C CNN
	1    5850 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1150 5850 1150
Wire Wire Line
	5850 1150 5850 1050
Text GLabel 5750 1900 0    60   Input ~ 0
S0
Text GLabel 5750 2000 0    60   Input ~ 0
S1
Text GLabel 5750 2100 0    60   Input ~ 0
S2
Text GLabel 5750 2200 0    60   Input ~ 0
S3
Text GLabel 5750 2300 0    60   Input ~ 0
OUT
Wire Wire Line
	5850 2300 5750 2300
Wire Wire Line
	5850 2200 5750 2200
Wire Wire Line
	5850 2100 5750 2100
Wire Wire Line
	5850 2000 5750 2000
Wire Wire Line
	5850 1900 5750 1900
$EndSCHEMATC
