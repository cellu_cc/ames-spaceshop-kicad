EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ARCSS_Breakouts
LIBS:ARCSS_Electromechanical
LIBS:ARCSS_Fab
LIBS:ARCSS_Sensors
LIBS:BC840-Breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_02x05_Odd_Even J1
U 1 1 5B451F14
P 1750 1000
F 0 "J1" H 1800 1300 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 1800 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch1.27mm_SMD" H 1750 1000 50  0001 C CNN
F 3 "" H 1750 1000 50  0001 C CNN
	1    1750 1000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR01
U 1 1 5B451F71
P 1450 700
F 0 "#PWR01" H 1450 550 50  0001 C CNN
F 1 "+3.3V" H 1450 840 50  0000 C CNN
F 2 "" H 1450 700 50  0001 C CNN
F 3 "" H 1450 700 50  0001 C CNN
	1    1450 700 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B451FED
P 1650 5650
F 0 "#PWR02" H 1650 5400 50  0001 C CNN
F 1 "GND" H 1650 5500 50  0000 C CNN
F 2 "" H 1650 5650 50  0001 C CNN
F 3 "" H 1650 5650 50  0001 C CNN
	1    1650 5650
	1    0    0    -1  
$EndComp
$Comp
L UMFT234XF U1
U 1 1 5B4FA91F
P 2050 5150
F 0 "U1" H 1900 5200 60  0000 C CNN
F 1 "UMFT234XF" H 2050 5650 39  0000 C CNN
F 2 "ARCSS-Fab:UMFT234XF" H 2100 5250 60  0001 C CNN
F 3 "" H 2100 5250 60  0001 C CNN
	1    2050 5150
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR03
U 1 1 5B4FAAD1
P 2500 4650
F 0 "#PWR03" H 2500 4500 50  0001 C CNN
F 1 "+3.3V" H 2500 4790 50  0000 C CNN
F 2 "" H 2500 4650 50  0001 C CNN
F 3 "" H 2500 4650 50  0001 C CNN
	1    2500 4650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5B4FABF0
P 1450 1350
F 0 "#PWR04" H 1450 1100 50  0001 C CNN
F 1 "GND" H 1450 1200 50  0000 C CNN
F 2 "" H 1450 1350 50  0001 C CNN
F 3 "" H 1450 1350 50  0001 C CNN
	1    1450 1350
	1    0    0    -1  
$EndComp
$Comp
L XTAL_32768 X1
U 1 1 5B4FADD2
P 2000 2050
F 0 "X1" H 2000 1900 60  0000 C CNN
F 1 "XTAL_32768" H 2000 2200 39  0000 C CNN
F 2 "ARCSS-Fab:LFXTAL003000" H 2000 2050 60  0001 C CNN
F 3 "" H 2000 2050 60  0001 C CNN
	1    2000 2050
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C2
U 1 1 5B4FAF95
P 2200 2300
F 0 "C2" H 2100 2200 60  0000 C CNN
F 1 "Capacitor" H 2200 2300 39  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2200 2300 60  0001 C CNN
F 3 "" H 2200 2300 60  0001 C CNN
	1    2200 2300
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C1
U 1 1 5B4FAFE5
P 1800 2300
F 0 "C1" H 1900 2200 60  0000 C CNN
F 1 "Capacitor" H 1800 2300 39  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 1800 2300 60  0001 C CNN
F 3 "" H 1800 2300 60  0001 C CNN
	1    1800 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 700  1450 800 
Wire Wire Line
	1450 800  1550 800 
Wire Wire Line
	1450 1200 1550 1200
Wire Wire Line
	1450 900  1450 1350
Wire Wire Line
	1550 1000 1450 1000
Connection ~ 1450 1200
Wire Wire Line
	1550 900  1450 900 
Connection ~ 1450 1000
Wire Wire Line
	2150 800  2050 800 
Wire Wire Line
	2050 900  2150 900 
Wire Wire Line
	1700 5600 1650 5600
Wire Wire Line
	1650 5600 1650 5650
Wire Wire Line
	2400 4750 2500 4750
Wire Wire Line
	2500 4750 2500 4650
Wire Wire Line
	2400 5000 2450 5000
Wire Wire Line
	2450 5100 2400 5100
$Comp
L GND #PWR05
U 1 1 5B4FB0A4
P 2000 2500
F 0 "#PWR05" H 2000 2250 50  0001 C CNN
F 1 "GND" H 2000 2350 50  0000 C CNN
F 2 "" H 2000 2500 50  0001 C CNN
F 3 "" H 2000 2500 50  0001 C CNN
	1    2000 2500
	1    0    0    -1  
$EndComp
$Comp
L Resistor R1
U 1 1 5B4FB70F
P 1850 3350
F 0 "R1" H 1850 3250 60  0000 C CNN
F 1 "Resistor" H 1850 3350 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 1850 3350 60  0001 C CNN
F 3 "" H 1850 3350 60  0001 C CNN
	1    1850 3350
	0    1    1    0   
$EndComp
$Comp
L LED D1
U 1 1 5B4FB778
P 1850 3800
F 0 "D1" H 1850 3900 50  0000 C CNN
F 1 "LED" H 1850 3700 50  0000 C CNN
F 2 "LEDs:LED_1206_HandSoldering" H 1850 3800 50  0001 C CNN
F 3 "" H 1850 3800 50  0001 C CNN
	1    1850 3800
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR06
U 1 1 5B4FB7C9
P 1850 4050
F 0 "#PWR06" H 1850 3800 50  0001 C CNN
F 1 "GND" H 1850 3900 50  0000 C CNN
F 2 "" H 1850 4050 50  0001 C CNN
F 3 "" H 1850 4050 50  0001 C CNN
	1    1850 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3050 1850 3150
Wire Wire Line
	1850 3550 1850 3650
Wire Wire Line
	1850 3950 1850 4050
Text GLabel 1850 3050 1    60   Input ~ 0
LED0
$Comp
L TactileSwitch S1
U 1 1 5B4FBFA3
P 2700 3600
F 0 "S1" H 2700 3450 60  0000 C CNN
F 1 "TactileSwitch" H 2700 3800 39  0000 C CNN
F 2 "ARCSS-Fab:PUSH_SWITCH" H 2700 3600 60  0001 C CNN
F 3 "" H 2700 3600 60  0001 C CNN
	1    2700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3200 2900 3200
Wire Wire Line
	2500 3500 2550 3500
Wire Wire Line
	2900 3200 2900 3500
Wire Wire Line
	2900 3500 2850 3500
Wire Wire Line
	2850 3700 2900 3700
Wire Wire Line
	2900 3700 2900 3850
Wire Wire Line
	2900 3850 2500 3850
Wire Wire Line
	2500 3850 2500 3700
Wire Wire Line
	2500 3700 2550 3700
$Comp
L GND #PWR07
U 1 1 5B4FC1D0
P 2700 3900
F 0 "#PWR07" H 2700 3650 50  0001 C CNN
F 1 "GND" H 2700 3750 50  0000 C CNN
F 2 "" H 2700 3900 50  0001 C CNN
F 3 "" H 2700 3900 50  0001 C CNN
	1    2700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3850 2700 3900
Connection ~ 2700 3850
Wire Wire Line
	2500 3200 2500 3500
Wire Wire Line
	2700 3100 2700 3200
Connection ~ 2700 3200
$Comp
L BT840 BT1
U 1 1 5B9AF845
P 5450 2000
F 0 "BT1" H 6100 2750 60  0000 C CNN
F 1 "BT840" H 6400 2750 60  0000 C CNN
F 2 "ARCSS-Fab:BT840F-NoBGA" H 5450 2000 60  0001 C CNN
F 3 "" H 5450 2000 60  0001 C CNN
	1    5450 2000
	1    0    0    -1  
$EndComp
Text GLabel 2150 800  2    60   Input ~ 0
SWDIO
Text GLabel 2150 900  2    60   Input ~ 0
SWDCLK
Text GLabel 7000 1650 2    60   Input ~ 0
SWDIO
Text GLabel 7000 1750 2    60   Input ~ 0
SWDCLK
Wire Wire Line
	2200 2450 2200 2400
Wire Wire Line
	1800 2450 2200 2450
Wire Wire Line
	1800 2450 1800 2400
Wire Wire Line
	2000 2450 2000 2500
Connection ~ 2000 2450
Wire Wire Line
	2150 2050 2200 2050
Wire Wire Line
	2200 1950 2200 2200
Wire Wire Line
	1850 2050 1800 2050
Wire Wire Line
	1800 1800 1800 2200
Wire Wire Line
	6850 1650 7000 1650
Wire Wire Line
	6850 1750 7000 1750
Text GLabel 4000 1850 0    60   Input ~ 0
XL1
Text GLabel 4000 1950 0    60   Input ~ 0
XL2
Wire Wire Line
	4050 1850 4000 1850
Wire Wire Line
	4050 1950 4000 1950
Text GLabel 7000 1950 2    60   Input ~ 0
LED0
Wire Wire Line
	6850 1950 7000 1950
Text GLabel 2300 1800 2    60   Input ~ 0
XL1
Text GLabel 2300 1950 2    60   Input ~ 0
XL2
Wire Wire Line
	2300 1950 2200 1950
Connection ~ 2200 2050
Wire Wire Line
	2300 1800 1800 1800
Connection ~ 1800 2050
Text GLabel 7000 1850 2    60   Input ~ 0
RESET
Wire Wire Line
	6850 1850 7000 1850
$Comp
L +3.3V #PWR08
U 1 1 5B9C01F0
P 7900 2200
F 0 "#PWR08" H 7900 2050 50  0001 C CNN
F 1 "+3.3V" H 7900 2340 50  0000 C CNN
F 2 "" H 7900 2200 50  0001 C CNN
F 3 "" H 7900 2200 50  0001 C CNN
	1    7900 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5B9C0219
P 6950 2450
F 0 "#PWR09" H 6950 2200 50  0001 C CNN
F 1 "GND" H 6950 2300 50  0000 C CNN
F 2 "" H 6950 2450 50  0001 C CNN
F 3 "" H 6950 2450 50  0001 C CNN
	1    6950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2250 6950 2250
Wire Wire Line
	6950 2250 6950 2450
Wire Wire Line
	6850 2350 7900 2350
Wire Wire Line
	7900 2350 7900 2200
Text GLabel 2700 3100 1    60   Input ~ 0
RESET
Text GLabel 4000 1750 0    60   Input ~ 0
UART_RX
Text GLabel 4000 1650 0    60   Input ~ 0
UART_TX
Text GLabel 2450 5000 2    60   Input ~ 0
UART_RX
Text GLabel 2450 5100 2    60   Input ~ 0
UART_TX
Wire Wire Line
	4050 1650 4000 1650
Wire Wire Line
	4050 1750 4000 1750
$Comp
L Resistor R3
U 1 1 5C8A91F1
P 4700 4500
F 0 "R3" H 4700 4400 60  0000 C CNN
F 1 "Resistor" H 4700 4500 39  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 4700 4500 60  0001 C CNN
F 3 "" H 4700 4500 60  0001 C CNN
	1    4700 4500
	0    1    1    0   
$EndComp
$Comp
L Resistor R2
U 1 1 5C8A923E
P 4700 3900
F 0 "R2" H 4700 3800 60  0000 C CNN
F 1 "Resistor" H 4700 3900 39  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4700 3900 60  0001 C CNN
F 3 "" H 4700 3900 60  0001 C CNN
	1    4700 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 4100 4700 4300
$Comp
L GND #PWR010
U 1 1 5C8A93C8
P 4700 4800
F 0 "#PWR010" H 4700 4550 50  0001 C CNN
F 1 "GND" H 4700 4650 50  0000 C CNN
F 2 "" H 4700 4800 50  0001 C CNN
F 3 "" H 4700 4800 50  0001 C CNN
	1    4700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4700 4700 4800
$Comp
L +3.3V #PWR011
U 1 1 5C8A943C
P 4700 3550
F 0 "#PWR011" H 4700 3400 50  0001 C CNN
F 1 "+3.3V" H 4700 3690 50  0000 C CNN
F 2 "" H 4700 3550 50  0001 C CNN
F 3 "" H 4700 3550 50  0001 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3700 4700 3550
Text GLabel 7000 2150 2    60   Input ~ 0
SENS
Wire Wire Line
	6850 2150 7000 2150
Text GLabel 4900 4200 2    60   Input ~ 0
SENS
Wire Wire Line
	4700 4200 4900 4200
Connection ~ 4700 4200
$EndSCHEMATC
